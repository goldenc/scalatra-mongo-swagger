package com.gitlab.goldenc

import com.gitlab.goldenc.swagger.JemstepSwagger
import org.scalatra.test.specs2._


// For more on Specs2, see http://etorreborre.github.com/specs2/guide/org.specs2.guide.QuickStart.html
class UserInfoSpec extends ScalatraSpec { def is =
  "GET /userinfo on UserInfo"                     ^
    "should return status 200"                  ! root200^
                                                end

  implicit val swagger = new JemstepSwagger  //nt req if not using swagger
  addServlet(new UserInfoController, "/user")

  def ping = get("user/ping") {
    body must_== "pong"
  }

  def root200 = get("user/userinfo") {
    status must_== 200
  }


  def userinfoId = get("/userinfo/:id/:name") {
    status must_== 200
  }

  def create = get("/create") {
    status must_== 200
  }



}
