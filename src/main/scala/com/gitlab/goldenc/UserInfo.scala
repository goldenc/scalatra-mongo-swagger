package com.gitlab.goldenc

import org.joda.time.DateTime

case class UserInfo(sub:  String,
                    firstName: String,
                    lastName:String,
                    email: String,
                    createdAt: DateTime,
                    identities: List[String],
                    organisationInfo: List[String])

case class User(name: String, email: String, id: Int  = 1)
