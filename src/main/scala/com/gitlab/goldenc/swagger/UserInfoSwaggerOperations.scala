package com.gitlab.goldenc

import org.json4s.{DefaultFormats, Formats}
import org.scalatra.swagger.SwaggerSupport

trait UserInfoSwaggerOperations  extends SwaggerSupport {

  protected implicit val jsonFormats: Formats = DefaultFormats.withBigDecimal
  protected val applicationDescription = "User Info API"

  val getUserInfo =
    (apiOperation[UserInfo]("getUserInfo")
      summary "Retrieve User info"
      tags("UserInfo")
      notes "Retrieve user info"
      parameter queryParam[Option[String]]("name").description("A name to search for"))

}
