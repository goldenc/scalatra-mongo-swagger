package com.gitlab.goldenc.swagger

import org.scalatra.ScalatraServlet
import org.scalatra.swagger.{ApiInfo, NativeSwaggerBase, Swagger}


class ResourcesApp(implicit val swagger: Swagger) extends ScalatraServlet with NativeSwaggerBase

object JemstepSwagger{
  val Info = ApiInfo(
    "The Jemstep API",
    "Docs for the Jemstep API",
    "http://scalatra.org",
    "apiteam@scalatra.org",
    "MIT",
    "http://opensource.org/licenses/MIT")
}
class JemstepSwagger extends Swagger(Swagger.SpecVersion, "1", JemstepSwagger.Info)
