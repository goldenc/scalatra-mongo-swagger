package com.gitlab.goldenc

import org.joda.time.DateTime
import org.scalatra._
import org.scalatra.json._
import org.scalatra.swagger._

class UserInfoController(implicit val swagger: Swagger)
  extends ScalatraServlet
    with NativeJsonSupport
    with UserInfoSwaggerOperations {

  before() {
    contentType = formats("json")
  }

  get("/ping") {
    Ok("pong")
  }

  get("/userinfo", operation(getUserInfo)) {
    Ok(dummyUserInfo)
  }

  get("/userinfo/:id/:name") {
    val maybeResponse = for {
      id <- params.get("id")
      name <- params.get("name")
    } yield {
      Ok(dummyUserInfo.copy(sub = id, firstName = name))
    }
    maybeResponse.getOrElse(BadRequest("missing userId"))

  }

  post("/create") {
    parsedBody.extractOpt[User] match {
      case Some(u) => Created(u)
      case _       => BadRequest("missing :")
    }
  }

  val dummyUserInfo = UserInfo("some id", "Golden", "Chiponda", "chipondagolden@gmail.com", DateTime.now, List("mock"), null)

}