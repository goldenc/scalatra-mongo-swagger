import javax.servlet.ServletContext

import com.gitlab.goldenc.UserInfoController
import com.gitlab.goldenc.swagger.{JemstepSwagger, ResourcesApp}
import org.scalatra.LifeCycle
class ScalatraBootstrap extends LifeCycle {

  implicit val swagger = new JemstepSwagger

  override def init(context: ServletContext) {
//    context.initParameters("org.scalatra.cors.allowedOrigins") = "http://petstore.swagger.io"
    context.mount(new UserInfoController, "/user", "userinfo")
    context.mount (new ResourcesApp, "/api-docs")
  }
}
